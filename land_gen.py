


field_size = 75
foodBirth = 100
scaling = 2
speed = 1
visitList = [ [.2 for i in range(field_size+1)] for j in range(field_size + 1)]
convoMat = [ [1 for i in range(field_size+1)] for j in range(field_size + 1)]
vectorField = [[[0,0] for i in range(field_size+1)] for j in range(field_size + 2)]
totalChunk = 5
fChunk = field_size // totalChunk

def calcVectorField():
    global visitList, vectorField
    
    for i in range(1, field_size -1):
        for j in range(1, field_size -1):
            biggest = 1
            for x in range(-1, 2):
                for y in range(-1, 2):
                    val = visitList[i][j] - visitList[i + x][j + y]
                    if val > biggest:
                        vectorField[i][j] = [i + x, j + y]

def erode():
    global visitList, waterfalls
    for i in range(field_size):
        for j in range(field_size):
            x = i
            y = j
            for z in range(10):
                if vectorField[x][y][0] != 0 and vectorField[x][y][1] != 0:
                    if visitList[x][y] > 0:
                        visitList[x][y] -= 0.1
                        x = vectorField[x][y][0]
                        y = vectorField[x][y][1]


for x in range(0,totalChunk):
    for y in range(0,totalChunk):
        val1 = random(0.85, 0.9)
        val2 = random(1.0, 1.2)
        for i in range(x * fChunk, (x + 1) * fChunk ):
            for j in range(y * fChunk, (y + 1) * fChunk):
                convoMat[i][j] = random(val1, val2)
                
                        
class Creature:
    
    def __init__(self, genome, startx, starty):
        self.genome = genome
        self.step = 0
        self.x = startx
        self.y = starty
        
        self.grassList = []
        for i in range(2):
            xv = random(0, 1)
            yv = random(0, 1)
            zv = random(.5, 1.2)
            self.grassList.append((xv, yv, zv))
            

        
    def drawSelf(self):
        global fieldList
        #fill(self.r,0,self.b,20)
        if visitList[self.x][self.y] > 1.2 and visitList[self.x][self.y] < 12:
            
            divVal = 1.0 / (visitList[self.x][self.y] ** .2)
            fill(83, 130 /divVal, 72, 70)
            z = visitList[self.x][self.y]/2
            translate(0,0,.5)
            for i in self.grassList:
                xv = i[0]
                yv = i[1]
                zv = i[2]
                translate(self.x + xv, self.y + yv, z)
                box(.2, .2, zv)
                translate(-self.x -xv, -self.y - yv, -z)
            translate(0,0,-.5)
            

    def iterate(self):
        global field, fieldList
        self.step = (self.step + 1) % len(self.genome)
        
        thisStep = self.genome[self.step]
                
        if thisStep == "U":
            self.y = (self.y + 1) % field_size
        elif thisStep == "D":
            self.y = (self.y - 1) % field_size
        elif thisStep == "L":
            self.x = (self.x - 1) % field_size
        elif thisStep == "R":
            self.x = (self.x + 1) % field_size
            
        elif thisStep == "Q":
            self.y = (self.y + 1) % field_size
            self.x = (self.x + 1) % field_size
        elif thisStep == "E":
            self.y = (self.y - 1) % field_size
            self.x = (self.x - 1) % field_size
        elif thisStep == "Z":
            self.x = (self.x - 1) % field_size
            self.y = (self.y + 1) % field_size
        elif thisStep == "C":
            self.x = (self.x + 1) % field_size
            self.y = (self.y - 1) % field_size
            

class FieldSquare:
    def __init__(self):
        self.food = 1

field = [ [FieldSquare() for j in range(field_size + 1)] for i in range(field_size + 1)]
fieldList = [ [[] for j in range(field_size + 1)] for i in range(field_size + 1)]
celllist = []
totalCells = 1000
totalProperties = 50
properties = "UDLRQEZC"
phenos = "0123456789"
drawcell = False
drawscreen = True
drawhealth = False
drawfood = False
alphaval = 10
foodcount = False
drawnut1 = False
drawnut2 = False     
FIGHTO = False
totalPreds = 100
waver = False
frame = 0
generation = 0
r = 0
radius = 10
iter = False
b = 300
hyperval = 9.0
cellTime = False
eroder = False
drawVector = False

def setup():
    frameRate(100)
    global celllist, predlist
    calcVectorField()

    size(1000, 1000, P3D)
    fieldList = []
    noStroke()
    for i in range(totalCells):
        s = ""
        p = ""
        startx = int(round(random(0, field_size)))
        starty = int(round(random(0, field_size)))
        for j in range(totalProperties):
            s = s + properties[int(round(random(0, len(properties) - 1)))]
        newcell = Creature(s, startx, starty)
        celllist.append(newcell)
        
        
    

def geneticTrait(fatherG, motherG):
    splitV = 25
    s = fatherG[0:splitV] + motherG[splitV:]
    return s
                


        
def draw():
    global celllist, predlist, fieldList, fieldPred, generation, r, convoMat, drawVector
    global field
    global foodBirth
    global speed
    global alphaval, drawfood, drawscreen, foodcount, drawhealth, drawcell, drawnut1, drawnut2, FIGHTO, frame
    scale(2,2,2)
    rotateX(.7)
    translate(225,320,100)
    translate(20,50,50)
    rotateZ(r)
    translate(-20,-50,-50)

    if drawscreen:
        background(0)
        
    
    if not drawVector:
        noStroke()
        if cellTime:
            fieldList = [ [[] for j in range(field_size + 1)] for i in range(field_size + 1)]
        
            cellbehave()
            for i in celllist:
                i.iterate()
    
        for i in celllist:
            i.drawSelf()
        
    
        directionalLight(255,255,255, 20,20,20)
        ambientLight(255,255,255)
    
    
        
        for i in range(1, len(visitList)-2):
            for j in range(1, len(visitList[i])-2):
                #translate(i, j, 0)
                # osil = visitList[i][j] / 10
                # fill(b*cos(osil)+128,b*cos(osil+PI*4/3)+128, b*cos(osil+PI*2/3)+128)
                
                if visitList[i][j] > 6.0: # black
                    fill(visitList[i][j] * 7)
                elif visitList[i][j] > 1.5: # green
                    divVal = 1.0 / (visitList[i][j] ** .4)
                    fill(83 * divVal, 139 * divVal, 72 * divVal)
                elif visitList[i][j] > .55: # sand
                    divVal = visitList[i][j]/0.9
                    fill(225 /divVal, 227 /divVal, 135 /divVal)
                elif visitList[i][j] < .5: # water
                    fill(100,100,255, 120)
                
                translate(i, j, 0)
                box(1, 1, visitList[i][j])
                translate(-i, -j, 0)
    
                
        if iter:
            normalizer()
        if waver:
            wave()
        if eroder:
            erode()
            
    else:
        stroke(255,0,0)
        strokeWeight(.5)
        for i in range(len(vectorField)):
            for j in range(len(vectorField[i])):
                
                if vectorField[i][j][0] != 0 and vectorField[i][j][1] != 0:
                    translate(i, j, 0)
                    line(i, j, visitList[i][j], vectorField[i][j][0], vectorField[i][j][1], visitList[ vectorField[i][j][0]] [vectorField[i][j][1]])
                    translate(-i, -j, 0)

    r += 0.01

#     savestring = "D:\land\\" + "4" + "--" + str(frame) + ".png"
#     frame += 1
#     save(savestring)

def normalizer():
    global visitList

    tempList = [[0 for i in range(len(visitList) -1)] for j in range(len(visitList)-1)]
    for i in range(1, len(visitList) - 2):
        for j in range(1, len(visitList) - 2):
            val = 0
            for x in range(-1, 2):
                for y in range(-1, 2):
                    val += visitList[i + x][j + y] * convoMat[i][j]

            tempList[i][j] = val / hyperval
    for i in range(1, len(visitList) - 2):
        for j in range(1, len(visitList) - 2):
            visitList[i][j] = tempList[i][j]
            
            
def wave():
    global visitList
    waveMat = [[1.1,1.1,1.1],
               [0,0,0],
               [-1.1,-1.1,-1.1]]
    tempList = [[0 for i in range(len(visitList) -1)] for j in range(len(visitList)-1)]
    for i in range(1, len(visitList) - 2):
        for j in range(1, len(visitList) - 2):
            val = 0
            
            for x in range(-1, 2):
                for y in range(-1, 2):
                    val += visitList[i + x][j + y] * waveMat[x + 1][y + 1]

            tempList[i][j] = val / hyperval
    for i in range(1, len(visitList) - 2):
        for j in range(1, len(visitList) - 2):
            if visitList[i][j] <= .6:
                visitList[i][j] = tempList[i][j]

        

def cellbehave():
    global celllist, fieldList, field, visitList
    births = []
    
    for i in celllist:
        fieldList[i.x][i.y].append(i)
        
        # if len(fieldList[i.x][i.y]) > 2:
            
        #     celllist.remove(i)
    

        visitList[i.x][i.y] += .01
        if len(fieldList[i.x][i.y]) > 1:
            c1 = fieldList[i.x][i.y][0]
            c2 = fieldList[i.x][i.y][1]
            
            fatherG = c1.genome
            motherG = c2.genome
            ng= geneticTrait(fatherG, motherG)
            births.append(Creature(ng, c1.x, c1.y))
                



            newG = i.genome
            if "V" in newG:
                splitV = newG.index("V")
            else:
                splitV = 50
                    
            newG = newG[splitV:-1] + "F" +  newG[0:splitV]
            births.append(Creature(newG, (i.x) % field_size, (i.y) % field_size))

    
    for i in births:
        if len(celllist) < 20000:
            celllist.append(i)

            
def keyPressed():
    global celllist, iter, hyperval, waver, cellTime, eroder, drawVector
    if key == "f":
        iter = not iter
    if key == "=":
        hyperval += .1
    if key == "-":
        hyperval -= .1
    if key == "w":
        waver = not waver
    if key == "c":
        cellTime = not cellTime
    if key == "e":
        eroder = not eroder
    if key == "1":
        calcVectorField()
    if key == "2":
        drawVector = not drawVector

    