field_size = 50
foodField =  [[[1 for i in range(field_size + 1)] for j in range(field_size + 1)] for k in range(field_size + 1)]
r = 0
health_gene = 5
max_health_gene = health_gene
explosion_size = 2

class Cell:
    
    def __init__(self, gene, pheno, startX, startY, startZ):
        self.gene = gene
        self.pheno = pheno
        self.x = startX
        self.y = startY
        self.z = startZ
        self.hp = 0
        self.maxHP = 0
        self.step = 0

        for i in range(len(gene)):
            if gene[i] == "H":
                self.hp += health_gene
                self.maxHP += max_health_gene

            
    def eat(self):
        self.maxHP -= 1
        self.hp = min(self.hp, self.maxHP)
        
            
    def iterate(self):
        global histList
        self.step = (self.step + 1) % len(self.pheno)
        thisStep = self.gene[self.step]
            
        if thisStep == "N":
            self.x = (self.x + explosion_size) % field_size
                
        elif thisStep == "S":
            self.x = (self.x - explosion_size) % field_size
            
        elif thisStep == "E":
            self.y = (self.y + explosion_size) % field_size
            
        elif thisStep == "W":
            self.y = (self.y - explosion_size) % field_size
            
        elif thisStep == "U":
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "D": 
            self.z = (self.z - explosion_size) % field_size
            
            
        elif thisStep == "Y":
            self.x = (self.x + explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "I":
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "G":
            self.x = (self.x + explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
        elif thisStep == "J":
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
            
        elif thisStep == "y":
            self.x = (self.x - explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "i":
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "g":
            self.x = (self.x - explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
        elif thisStep == "j":
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size    
            
            
        elif thisStep == "U":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size
            
        elif thisStep == "O":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            
        elif thisStep == "V":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            
        elif thisStep == "N":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size  
            
            
        elif thisStep == "u":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "o":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
        elif thisStep == "v":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
        elif thisStep == "r":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
            
        elif thisStep == "n":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z - explosion_size) % field_size
        
        elif thisStep == "p":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "P":
            self.x = (self.x + explosion_size) % field_size
            self.y = (self.y - explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
            
        elif thisStep == "R":
            self.x = (self.x - explosion_size) % field_size
            self.y = (self.y + explosion_size) % field_size
            self.z = (self.z + explosion_size) % field_size
        
        histList.append(History(self.x, self.y, self.z))
        self.eat()
        
class History():
    
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.age = .1
        self.risingRate = 0
    def drawSelf(self):
        fill(255 //self.age, int(random(120,150))// self.age, 30//self.age, 100//self.age)
        translate(self.x, self.y, self.z)
        noStroke()
        box(self.age/10 + 1)
        translate(-self.x, -self.y, -self.z)
    def iterate(self):
        self.age += 0.1
        self.x -= self.risingRate / 10
        self.y -= self.risingRate
        self.risingRate += 0.065
        
        
        
cellList = []
phenos = "0987654321"
genes = "NSEWUDHSXVYIGJyigjUOVNuovnPRrZM"
properties = 60
totalCells = 300
histList = []
fallingRate = -1.3
explosionRadius = 1
s = 0

def setup():
    global cellList
    cellList = []
    
    size(600,600,P3D)
    for i in range(totalCells):
        s = ""
        p = ""
        startx = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
        starty = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
        startz = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
        for j in range(properties):
            s = s + genes[int(round(random(0, len(genes) - 1)))]
            p = p + phenos[int(round(random(0, len(phenos) - 1)))]
        newCell = Cell(s, p, startx, starty, startz)
        cellList.append(newCell)
    noStroke()
    
    
def draw():
    global fieldList, r, fallingRate, s
    background(255)
    translate(275,275,300)
    translate(25,25,25)
    rotateY(r)
    #rotateX(r)
    scale(3,3,3)
    translate(-25,-25,-25)
    #noStroke()
    
    for i in histList:
        i.drawSelf()
        
        
    for i in cellList:
        i.iterate()
        i.iterate()
        i.iterate()

        if i.y < field_size - 1:
            i.y = (i.y + 1)


    for i in histList:
        i.iterate()
        i.y += fallingRate
        if i.age > 20:
            histList.remove(i)
    fallingRate += 0.08

   
    fieldList = [ [[[] for i in range(field_size + 1)] for j in range(field_size + 1)] for k in range(field_size + 1)]     
    cellBehave()
    cellDeath()
    directionalLight(150,150,150,50,50,25)
      
    ambientLight(250, 150, 30)
    r += .005
    #s += .04
    stroke(150)
    strokeWeight(0.5)
    noFill()
    translate(25,25,25)
    box(50)
    

def geneticTrait(fatherG, motherG, fatherP, motherP):

    #splitV = int(random(0, totalProperties))
    if "V" in fatherG:
        splitV = fatherG.index("V")
    elif "V" in motherG:
        splitV = motherG.index("V")
    else:
        splitV = properties//2
    s = fatherG[0:splitV] + motherG[splitV:]
    p = fatherP[0:splitV] + motherP[splitV:]
    return s, p     



def cellBehave():
    global cellList, fieldList, foodField, interaction_list, fight_list, friend_list
    births = []
    for i in cellList:
        fieldList[i.x][i.y][i.z].append(i)
            
        if len(fieldList[i.x][i.y][i.z]) > 1:
            c1 = fieldList[i.x][i.y][i.z][0]
            c2 = fieldList[i.x][i.y][i.z][1]
            
            if (c1.hp > 10 and c2.hp > 10):
                fatherG, fatherP = c1.gene, c1.pheno
                motherG, motherP = c2.gene, c2.pheno
                ng, np = geneticTrait(fatherG, motherG, fatherP, motherP)
                c1.hp = (c1.hp // 2)
                c2.hp = (c2.hp // 2)
                births.append(Cell(ng, np, c1.x, c1.y, c1.z))
                births[0].maxHP = c1.maxHP
            
            
        if i.hp > 20:
            newG = i.gene
            newP = i.pheno
            if "V" in newG:
                splitV = newG.index("V")
            else:
                splitV = 150
            newG = newG[splitV:-1] + "V" + newG[0:splitV]
            newP = newP[splitV:] + newP[0:splitV]
            births.append(Cell(newG, newP, i.x, i.y, i.z))
            i.hp = i.hp // 3
            births[0].hp = births[0].hp // 3
            births[0].maxHP = i.maxHP
   
    for i in births:
        if len(cellList) < 400:
            cellList.append(i)
            
            
            
def cellDeath():
    global cellList
    for i in cellList:
        if i.hp <= 0:
            
            cellList.remove(i)
            
def keyPressed():
    global inverse, cellList, interacts, histList, fallingRate, s, histList, r
    if key == "i":
        inverse = not inverse
    if key == "k":
        cellList = []
        histList = []
    if key == "r":
        cellList = []
        histList = []
        for i in range(totalCells):
            s = ""
            p = ""
            startx = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
            starty = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
            startz = int(round(random(25 - explosionRadius, 25 + explosionRadius)))
            for j in range(properties):
                s = s + genes[int(round(random(0, len(genes) - 1)))]
                p = p + phenos[int(round(random(0, len(phenos) - 1)))]
            newCell = Cell(s, p, startx, starty, startz)
            cellList.append(newCell)
        fallingRate = -1.3
