## About

This is a collection of programs that were all based on the same initial code.
First is the file "Cellular Automata" which is really the bread and butter of
the other two files seen here.  The simulation uses a formal grammar with some
re-writing rules to create kind of a neat, extremely complicated system based on
a few simple rules.  The cells can fight, reproduce, share food, or eat.  They
also live for a finite amount of time no matter what.

The explosion file is based on the original work with the cellular automata.
The work is generalized to 3D and some major tweaks were put in place to give
the cells a very different type of behavior, but under the hood it's still the
same rules that govern the original 2D version

Finally, the work was adapted for use in a land generation algorithm. At first
the cells themselves generated the land.  In the current version the cells work
together with a set of convolution filters to generate land, and flora for kind
of a minecrafty looking world.


## Setup

All of the 3 files are made to run in processing, which can be found [here](https://processing.org/).

