
class FieldSquare:
    
    def __init__(self):
        self.food = 1
        
    def getFood(self):
        return self.food
    
    
class Cell:
    
    def __init__(self, genome, phenom, startX, startY):
        self.genome = genome
        self.phenom = phenom
        
        self.strength = 0
        self.maxHP = 0
        self.hp = 0
        self.step = 0
        
        self.x = startX
        self.y = startY
        
        self.currBehave = ""
            
        
        if "agg" in genome:
            self.splitV = genome.index("agg")
        elif "gaa" in genome:
            self.splitV = genome.index("gaa")
        else:
            self.splitV = "INF"
        
        
        self.r = int(self.phenom[0:200]) % 255
        self.b = int(self.phenom[200:400]) % 255
        self.g = int(self.phenom[400:600]) % 255
        
        for i in range(len(genome) - 2):
            g = genome[i:i+3]
            if g == "gtt":
                self.maxHP += 300
                self.hp += 90
            elif g == "caa":
                self.strength += 10


    def drawSelf(self):
        fill(self.r, self.g, self.b, 30)
        rect(self.x*2, self.y*2, 2, 2)
        
    def drawChaos(self):
        fill(self.r, self.g, self.b, 30)
        x = 900
        y = 600

        for i in range(len(self.genome)):
            curr = self.genome[i]
            rect(x, y, 1, 1)
            if curr == 'a':
                x = (x + 600) / 2 #0, 0 case
                y = y / 2
            elif curr == 'g':
                x = (x + 1200) / 2 # 0, 1 case
                y = y / 2
            elif curr == 't':
                x = (x + 600) / 2 # 1, 0 case
                y = (y + 600) / 2
            elif curr == 'c':
                x = (x + 1200) / 2 # 1, 1 case
                y = (y + 600) / 2
            
        
    def eat(self):
        if field[self.x][self.y] > 0:
            self.hp += (100 * field[self.x][self.y].food)
            self.hp = min(self.hp, self.maxHP)
        field[self.x][self.y].food = 0
        self.maxHP -= 1
                
                
    def iterate(self):
        self.step = (self.step + 1) % (len(self.phenom) - 2)
        step = self.step
        g = self.genome[step:step + 3]
        
        
        if g == "acc" or g == "taa":
            self.y = (self.y + 1) % fieldSize
        elif g == "att" or g == "gcc":
            self.y = (self.y - 1) % fieldSize
        elif g == "ctt" or g == "aaa":
            self.x = (self.x - 1) % fieldSize
        elif g == "ttt" or g == "tgg":
            self.x = (self.x + 1) % fieldSize
                
        elif g == "cgg" or g == "ccc":
            self.currBehave = "REP"
        elif g == "tcc":
            self.currBehave = "FIG"
        elif g == "ggg":
            self.currBehave = "SHA"
        
        self.eat()
                


### GLOBALS DECLARED HERE"
fieldSize = 300
totalCells = 5000
totalProps = 600
props = 'agtc'
phenos = "0987654321"
field = [ [FieldSquare() for j in range(fieldSize + 1)] for i in range(fieldSize + 1)]
fieldList = [ [[] for j in range(fieldSize + 1)] for i in range(fieldSize + 1)]
frame = 0
generation = 0
d = 0
cellList = []
drawC = False
dALL = False
def setup():
    global cellList
    frameRate(100)
    noStroke()
    size(1200,600)
    for i in range(totalCells):
        s = ""
        p = ""
        startX = int(round(random(0, fieldSize)))
        startY = int(round(random(0, fieldSize)))
        for j in range(totalProps):
            s = s + props[int(random(4))]
            p = p + phenos[int(random(0, len(phenos) - 1))]
        newCell = Cell(s, p, startX, startY)
        cellList.append(newCell)
    fill(0,0,0)
    
    
    
def simularity(c1, c2):
    """ determines how similar two cells are based on their phenotype, this is used to determine if a 
    cell wil fight or reproduce with other cells"""
    t = 0
    for i in range(len(c1.phenom)):
        if c1.phenom[i] == c2.phenom[i]:
            t += 1
    return t
          
def newCellBirth(c1, c2):
    fatherG = c1.genome
    fatherP = c1.phenom
    motherG = c2.genome
    motherP = c2.phenom
    x = c1.x
    y = c1.y
    
    if c1.splitV == "INF" and c2.splitV == "INF":
        return None

    elif c1.splitV != "INF":
        splitV = c2.splitV
        
    elif c2.splitV != "INF":
        splitV = c1.splitV


    newGene = fatherG[0:splitV] + motherG[splitV:]
    newPhen = fatherP[0:splitV] + motherP[splitV:]
    newCell = Cell(newGene, newPhen, x, y)
    return newCell
    
def draw():
    fill(0,0,0,50)
    rect(0,0,600,600)
    global fieldList, drawC, d, dALL
    for i in cellList:
        i.drawSelf()
        
    # cellBehave()
    # cellDeath()
    # fieldList = [ [[] for j in range(fieldSize + 1)] for i in range(fieldSize + 1)]
    #     for i in cellList:
    #         i.iterate()

    for j in range(1):
        cellBehave()
        cellDeath()
        fieldList = [ [[] for j in range(fieldSize + 1)] for i in range(fieldSize + 1)]
        for i in cellList:
            i.iterate()
        
        
    if drawC:
        cellList[d].drawChaos()
        
    if dALL:
        for i in cellList:
            i.drawChaos()

    
        
def cellBehave():
    global cellList, fieldList, field
    births = []
    for i in cellList:
        fieldList[i.x][i.y].append(i)
        
        if len(fieldList[i.x][i.y]) > 2:
            field[(i.x + 1) % fieldSize][i.y].food += (i.hp // 5)
            field[(i.x - 1) % fieldSize][i.y].food += (i.hp // 5)
            field[i.x][(i.y + 1) % fieldSize].food += (i.hp // 5)
            field[i.x][(i.y - 1) % fieldSize].food += (i.hp // 5)
            field[i.x][i.y].food += (i.hp // 5)
            
            cellList.remove(i)
            
        if len(fieldList[i.x][i.y]) > 1:
            c1 = fieldList[i.x][i.y][0]
            c2 = fieldList[i.x][i.y][1]
            simu = simularity(c1, c2)
            
            if (c1.currBehave == "REP" or c2.currBehave == "REP") and simu < (totalProps // 2):
                NCB = newCellBirth(c1, c2)
                if NCB != None:
                    births.append(NCB)
                    
            elif (c1.currBehave == "FIG" or c2.currBehave == "FIG") and simu < ((totalProps // 6) * 5):
                if c1.strength > c2.hp:
                    c1.hp += c2.hp
                    c2.hp = 0
                elif c2.strength > c1.hp:
                    c2.hp += c1.hp
                    c1.hp = 0
                else:
                    c1.hp -= c2.strength
                    c2.hp -= c1.strength
                    
            # elif (c1.currBehave == "SHA" or c2.currBehave == "SHA") and simu < (totalProps // 2):
            #     if int((c1.hp / (c1.maxHP + 1)) * 100) < 30 or int((c2.hp / (c2.maxHP + 1)) * 100) < 30:
            #         print('share happened')
            #         totalFood = c1.hp + c2.hp
            #         c1.hp = totalFood//2
            #         c2.hp = totalFood//2
                    
                    
    for i in births:
        #print(len(births))
        if len(cellList) < 3000:
            cellList.append(i)
    
    
def cellDeath():
    global cellList
    for i in cellList:
        if i.hp <= 0:
            field[(i.x + 1) % fieldSize][i.y].food += 1
            field[(i.x - 1) % fieldSize][i.y].food += 1
            field[i.x][(i.y + 1) % fieldSize].food += 1
            field[i.x][(i.y - 1) % fieldSize].food += 1
            field[i.x][i.y].food += 1 
            cellList.remove(i)
               # The current behavioural traits, some no-ops left in for future behaviors that might be added
"""
'gtt'   = health
'caa',  = strength
'acc'   = up
'att'   = down
'ctt',  = left
'ttt',  = right
'cgg',  = reproduce
'agg',  = split
'tcc',  = fight
'ggg',  = share
'gaa',  = split
'taa',  = up
'gcc',  = down
'aaa',  = left
'tgg',  = right
'ccc',  = reproduce

"""           
    
def keyPressed():
    global cellList, drawC, d, dALL
    if key == "l":
        print(len(cellList))
    if key == "c":
        drawC = not drawC
    if key == "=":
        d += 1
        fill(0)
        rect(600, 0, 1200, 1200)
        
    if key == "-":
        d -= 1
        fill(0)
        rect(600, 0, 1200, 1200)
    if key == "d":
        dALL = not dALL
    
    
    

                                        